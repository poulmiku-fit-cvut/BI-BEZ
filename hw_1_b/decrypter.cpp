// Mikulas Poul - poulmiku

#include <iostream>
#include <openssl/evp.h>
#include <string>

using namespace std;

uint8_t hex_to_val(char x) {
    // I couldn't be bother to google how to do this properly.

    switch (x) {
        case 'a':
        case 'A':
            return 10;
        case 'b':
        case 'B':
            return 11;
        case 'c':
        case 'C':
            return 12;
        case 'd':
        case 'D':
            return 13;
        case 'e':
        case 'E':
            return 14;
        case 'f':
        case 'F':
            return 15;
        case '0':
            return 0;
        case '1':
            return 1;
        case '2':
            return 2;
        case '3':
            return 3;
        case '4':
            return 4;
        case '5':
            return 5;
        case '6':
            return 6;
        case '7':
            return 7;
        case '8':
            return 8;
        case '9':
            return 9;
        default:
            return 0;

    }
}

int main(void) {

    string ct1;  // ciphertext 1
    string ct2;  // ciphertext 2
    string pt("abcdefghijklmnopqrstuvwxyz0123");  // plaintext

    cout << "Enter first cipher text: " << endl;

    cin >> ct1;

    if (ct1.length() > 1000) {
        cout << "That's too long." << endl;
        exit(1);
    }

    cout << "Enter second cipher text: " << endl;

    cin >> ct2;

    if (ct2.length() > 1000) {
        cout << "That's too long." << endl;
        exit(1);
    }

    cout << "Enter the plaintext (" << pt << ") if EOF: " << endl;

    if (!cin.eof()) {
        string tmp;
        cin >> tmp;

        if (tmp.length()) {
            pt = tmp;
        }
    }

    if (pt.length() > 500) {
        cout << "That's too long." << endl;
        exit(1);
    }


    uint32_t len = (uint32_t) min(min(ct1.length() / 2, ct2.length() / 2), pt.length());

    if (len < ct1.length() / 2 or len < ct2.length() / 2) {
        cout << "Can only decipher " << len << " characters." << endl;
    }

    cout << endl << "The plaintext is: ";

    for (uint32_t i = 0; i < len; ++i) {
        uint8_t x_1 = hex_to_val(ct1[2 * i]) * (uint8_t) 16 + hex_to_val(ct1[2 * i + 1]);
        uint8_t x_2 = hex_to_val(ct2[2 * i]) * (uint8_t) 16 + hex_to_val(ct2[2 * i + 1]);

        cout << (char) (x_1 ^ x_2 ^ (uint8_t) pt[i]);
    }

    cout << endl;

    return 0;
}
