// Mikulas Poul - poulmiku

#include <stdlib.h>
#include <openssl/evp.h>
#include <string.h>

int main(void) {

    unsigned char pt[1024] = "Orphan Son Of A Whore Scotsman Dropped In The Middle";  // plaintext
    unsigned char ct[1024];  // ciphertext

    unsigned char pt2[1024] = "abcdefghijklmnopqrstuvwxyz0123456789";  // plaintext
    unsigned char ct2[1024];  // ciphertext


    unsigned char key[EVP_MAX_KEY_LENGTH] = "HowDoesABastard?";  // encryption and decryption key
    unsigned char iv[EVP_MAX_IV_LENGTH] = "asldfjasldf sdf";  // initialization vector
    const char cipherName[] = "RC4";
    const EVP_CIPHER * cipher;

    int ptLength = strlen((const char*) pt);
    int pt2Length = strlen((const char*) pt2);
    int ctLength = 0;
    int ct2Length = 0;
    int tmpLength = 0;
    int res;

    OpenSSL_add_all_ciphers();
    /* ciphers and hashes could be loaded using OpenSSL_add_all_algorithms() */

    cipher = EVP_get_cipherbyname(cipherName);
    if(!cipher) {
        printf("Cipher %s not found.\n", cipherName);
        exit(1);
    }

    EVP_CIPHER_CTX *ctx; // context structure
    ctx = EVP_CIPHER_CTX_new();
    if(ctx == NULL) exit(2);

    printf("pt: %s\n", pt);

    /* Encryption */
    res = EVP_EncryptInit_ex(ctx, cipher, NULL, key, iv);  // context init - set cipher, key, init vector
    if(res != 1) exit(3);
    res = EVP_EncryptUpdate(ctx,  ct, &tmpLength, pt, ptLength);  // encryption of pt
    if(res != 1) exit(4);
    ctLength += tmpLength;
    res = EVP_EncryptFinal_ex(ctx, ct + ctLength, &tmpLength);  // get the remaining ct
    if(res != 1) exit(5);
    ctLength += tmpLength;

    tmpLength = 0;

    res = EVP_EncryptInit_ex(ctx, cipher, NULL, key, iv);  // context init - set cipher, key, init vector
    if(res != 1) exit(3);
    res = EVP_EncryptUpdate(ctx,  ct2, &tmpLength, pt2, pt2Length);  // encryption of pt
    if(res != 1) exit(4);
    ct2Length += tmpLength;
    res = EVP_EncryptFinal_ex(ctx, ct2 + ct2Length, &tmpLength);  // get the remaining ct
    if(res != 1) exit(5);
    ct2Length += tmpLength;

    printf ("Encrypted %d characters.\n", ctLength);

//    /* Decryption */
//    res = EVP_DecryptInit_ex(ctx, cipher, NULL, key, iv);  // context init for decryption
//    if(res != 1) exit(3);
//    res = EVP_DecryptUpdate(ctx, pt, &tmpLength,  ct, ctLength);  // decrypt ct
//    if(res != 1) exit(4);
//    ptLength += tmpLength;
//    res = EVP_DecryptFinal_ex(ctx, pt + ptLength, &tmpLength);  // get the remaining plaintext
//    if(res != 1) exit(5);
//    ptLength += tmpLength;

    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);

    /* Print out the encrypted and decrypted texts.
       Ciphertext will probably not be printable! */

    printf("CT: ");
    /* Vypsani vysledneho hashe */
    for (int i = 0; i < ctLength; i++) {
        printf("%02x", ct[i]);
    }
    printf(" pro: <secret>\n");

    printf("CT: ");
    /* Vypsani vysledneho hashe */
    for (int i = 0; i < ct2Length; i++) {
        printf("%02x", ct2[i]);
    }
    printf(" pro: %s\n", pt2);

    exit(0);
}