// Mikulas Poul - poulmiku

#include <iostream>
#include <fstream>
#include <openssl/evp.h>

using namespace std;

uint32_t read_big_endian(ifstream & file) {
    // there's probably a better way to do this, didn't care enough to google
    char x[4];

    file.read(x, 4);

    return (uint32_t)(unsigned char) x[0] | (uint32_t)(unsigned char) x[1] << 8 | (uint32_t)(unsigned char) x[2] << 16 | (uint32_t)(unsigned char) x[3] << 24;
}

void write_big_endian(uint32_t length, ofstream & file) {
    // there's probably a better way to do this, didn't care enough to google
    char x[4];

    x[0] = (char) ((length ) & 0xFF);
    x[1] = (char) ((length >> 8) & 0xFF);
    x[2] = (char) ((length >> 16) & 0xFF);
    x[3] = (char) ((length >> 24) & 0xFF);

    file.write(x, 4);
}

bool replace(std::string& str, const std::string& from, const std::string& to) {
    // replace string in string, returns true if from string was in it
    size_t start_pos = str.find(from);
    if(start_pos == std::string::npos)
        return false;
    str.replace(start_pos, from.length(), to);
    return true;
}

string new_filename(bool encrypt, bool decrypt, bool ecb, bool cbc, string filename) {
    // generates a new filename from the original (ads the _dec, _cbc, _ecb and so on)
    string new_filename = filename;

    if (decrypt) {
        if (!replace(new_filename, ".bmp", "_dec.bmp")) {
            new_filename += "_dec.bmp";
        }
    } else {
        string add = ecb ? "ecb" : "cbc";

        if (!replace(new_filename, ".bmp", "_" + add + ".bmp")) {
            new_filename += "_" + add + ".bmp";
        }
    }

    return new_filename;
}


int action(bool encrypt, bool decrypt, bool ecb, bool cbc, string filename, uint32_t length, uint32_t start, ifstream & file) {
    // does the specific action from the command line
    string new_fl_name = new_filename(encrypt, decrypt, ecb, cbc, filename);

    // copy headers
    file.seekg(0, ios::beg);
    char * headers = new char[start];
    file.read(headers, start);

    ofstream new_fl;
    new_fl.open(new_fl_name, ios::binary);
    new_fl.write(headers, start);
    delete [] headers;

    // init ssl
    OpenSSL_add_all_ciphers();
    EVP_CIPHER_CTX *ctx;
    ctx = EVP_CIPHER_CTX_new();
    if(ctx == NULL) exit(2);

    int res;
    unsigned char key[] = "How Does A Bastard Orphan";
    unsigned char iv[] = "abcdefghijklmnopqrstuvwxyz0123";

    res = EVP_CipherInit(ctx, cbc ? EVP_des_cbc() : EVP_des_ecb(), key, iv, (int) encrypt);

    if (res != 1) {
        cout << "Problem with the encryption/decryption." << endl;
        return 3;
    }

    uint32_t offset = start;
    uint32_t block_size = 500;
    char * data = new char[block_size];
    unsigned char * cipher = new unsigned char[block_size * 3];
    int cipher_length = 0;
    uint32_t new_length = start;

    // main loop
    while(offset < length) {
        uint32_t read_length = (uint32_t) min(block_size, length - offset); // handles end of file

        file.read(data, read_length);
        offset += read_length;

        res = EVP_CipherUpdate(ctx,  cipher, &cipher_length, reinterpret_cast<unsigned char *>(data), read_length);  // encryption of orig data

        if(res != 1) {
            cout << "Problem with the encryption/decryption." << endl;
            return 3;
        }

        new_fl.write(reinterpret_cast<char *>(cipher), cipher_length);
        new_length += cipher_length;
    }

    // encrypt/decrypt the rest
    res = EVP_CipherFinal(ctx, cipher, &cipher_length);

    if(res != 1) {
        cout << "Problem with the encryption/decryption." << endl;
        return 3;
    }

    new_fl.write(reinterpret_cast<char *>(cipher), cipher_length);
    new_length += cipher_length;

    // save new length, clear up
    new_fl.seekp(2, ios::beg);
    write_big_endian(new_length, new_fl);

    delete [] data;
    delete [] cipher;
    file.close();
    new_fl.close();
    return 0;
}

int main(int argc, char * argv[]) {

    // arguments validation

    if (argc > 1 and string(argv[1]) == "--help") {
        cout << "Usage: " << endl;
        cout << "./a.out (-d|-e) (ecb|cbc) <filename>" << endl;
        return 1;
    }

    if (argc != 4) {
        cout << "Invalid number of arguments, see --help for usage" << endl;
        return 1;
    }

    if (string(argv[1]) != "-d" and string(argv[1]) != "-e") {
        cout << "Invalid arguments, see --help for usage" << endl;
        return 1;
    }

    if (string(argv[2]) != "ecb" and string(argv[2]) != "cbc") {
        cout << "Invalid arguments, see --help for usage" << endl;
        return 1;
    }

    // file validation

    ifstream file;
    file.open(argv[3], ios::binary | ios::ate);

    if (not file.is_open()) {
        cout << "File doesn't exist" << endl;
        return 1;
    }

    uint64_t actual_length = (uint64_t) file.tellg();

    if (actual_length < 14) {
        cout << "The file isn't long enough to even contain basic headers." << endl;
        file.close();
        return 1;
    }

    file.seekg(2, ios::beg);

    uint32_t length = read_big_endian(file);

    if (length != actual_length) {
        cout << "There are incorrect headers - the length attribute doesn't match the actual length." << endl;
        file.close();
        return 1;
    }

    file.seekg(10, ios::beg);

    uint32_t start = read_big_endian(file);

    if (start > actual_length) {
        cout << "Start is after the end of the file." << endl;
        file.close();
        return 1;
    }

    return action(string(argv[1]) == "-e", string(argv[1]) == "-d",
                  string(argv[2]) == "ecb", string(argv[2]) == "cbc",
                  argv[3], length, start, file);
}
