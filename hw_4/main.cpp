// Mikulas Poul - poulmiku

#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <openssl/ssl.h>
#include <openssl/pem.h>
#include <netdb.h>
#include <sstream>

using namespace std;

#define BUFFER_SIZE 1024

string split(const string & s, char delim) {
    // A helper function to get hostname from URL
    stringstream ss;
    ss.str(s);
    string item;
    getline(ss, item, delim);
    return item;
}

int url_to_ip_and_domain_and_path(string url, string & ip, string & domain, string & path) {
    // Function that gets IP address, domain and path of the request
    struct addrinfo hints, * servinfo, * p;
    struct sockaddr_in * h;
    int rv;
    string https = "https";

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC; // use AF_INET6 to force IPv6
    hints.ai_socktype = SOCK_STREAM;

    domain = url;
    domain.replace(0, 8, "");
    path = domain;
    domain = split(domain, '/');

    path.replace(0, domain.length(), "");

    if ((rv = getaddrinfo(domain.data(), "https", &hints, &servinfo)) != 0) {
        return 0;
    }

    // loop through all the results and connect to the first we can
    for (p = servinfo; p != NULL; p = p->ai_next) {
        h = (struct sockaddr_in *) p->ai_addr;
        ip = inet_ntoa(h->sin_addr);
    }

    freeaddrinfo(servinfo); // all done with this structure
    return 1;
}

int action(string url, string certificate_path, string output_file) {
    int sockfd;
    struct sockaddr_in servaddr;
    char buffer[BUFFER_SIZE];
    string ip_address;
    string path;
    string domain;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (!url_to_ip_and_domain_and_path(url, ip_address, domain, path)) {
        cout << "Can't get IP from address" << endl;
        return 1;
    }

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(ip_address.data()); //ip address
    servaddr.sin_port = htons(443); // port

    if (connect(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) != 0) {
        cout << "Unable to connect" << endl;
    }

    SSL_library_init();

    SSL_CTX * ssl_ctx = SSL_CTX_new(SSLv23_client_method());
    if (!ssl_ctx) {
        cout << "SSL_CTX_new failed" << endl;
        return 2;
    }

    SSL_CTX_set_default_verify_paths(ssl_ctx);

    SSL_CTX_set_options(ssl_ctx, SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_TLSv1);

    SSL * ssl = SSL_new(ssl_ctx);
    if (!ssl) {
        cout << "SSL_new failed" << endl;
        return 2;
    }

    if (!SSL_set_fd(ssl, sockfd)) {
        cout << "SSL_set_fd failed" << endl;
        return 2;
    }

    SSL_set_cipher_list(ssl,"ALL:!ECDHE-RSA-AES256-GCM-SHA384");
    if (SSL_connect(ssl) <= 0) {
        cout << "SSL_connect failed" << endl;
        return 2;
    }

    if (SSL_get_verify_result(ssl) != X509_V_OK) {
        cout << "SSL_get_verify_result failed" << endl;
        return 2;
    }

    X509 * cert = SSL_get_peer_certificate(ssl);
    if (!cert) {
        cout << "SSL_get_cert failed" << endl;
        return 2;
    }

    FILE * file_PEM = fopen(certificate_path.data(), "w");
    if (!PEM_write_X509(file_PEM, cert)) {
        cout << "PEM_write failed" << endl;
        return 3;
    }
    fclose(file_PEM);

    snprintf(buffer, BUFFER_SIZE,
             ("GET " + path + " HTTP/1.1\r\nConnection: close\r\nHost: " + domain + "\r\n\r\n").data());

    if (SSL_write(ssl, buffer, strlen(buffer) + 1) <= 0) {
        cout << "SSL_write failed" << endl;
        return 3;
    }

    int res;
    int datalen = 0;
    FILE * file_output = fopen(output_file.data(), "w");
    while ((res = SSL_read(ssl, buffer, BUFFER_SIZE)) > 0) {
        fwrite(buffer, sizeof(char), res, file_output);
        datalen += res;
    }
    fprintf(file_output, "\n");
    fclose(file_output);

    auto ssl_cipher = SSL_get_current_cipher(ssl);

    cout << "Current cipher name is " << SSL_CIPHER_get_name(ssl_cipher) << endl;

    // example ECDHE-RSA-AES256-GCM-SHA384 (when forbidden, AES128 is used instead)
    // ECDGE-RSA - extended Diffie-Hellman exchange that uses elliptic curves, signed by RSA, for key exchange
    // AES256 - the cipher itself
    // GCM - operating mode of the cipher
    // SHA384 - hash function for authority signing

    cout << "Available ciphers: " << endl;
    int prior = 0;
    const char * cipher_name;
    while ((cipher_name = SSL_get_cipher_list(ssl, prior))) {
        cout << cipher_name << endl;
        prior++;
    }

    SSL_shutdown(ssl);
    shutdown(sockfd, 2);
    SSL_free(ssl);
    SSL_CTX_free(ssl_ctx);

    return 0;
}


int main(int argc, char * argv[]) {

    // arguments validation

    if (argc > 1 and string(argv[1]) == "--help") {
        cout << "Usage: " << endl;
        cout
                << "./a.out [<output_file='output.pem'> [<data_file='output.txt'> [[<url='https://fit.cvut.cz/student/odkazy'>]]]"
                << endl;
        cout << "Cipher name is applicable for -e only" << endl;
        return 1;
    }

    if (argc > 4) {
        cout << "Invalid number of arguments, see --help for usage" << endl;
        return 1;
    }

    string certificate_file = "output.pem";
    string url = "https://fit.cvut.cz/student/odkazy";
    string output_file = "output.txt";

    if (argc > 1) {
        certificate_file = argv[1];
    }

    if (argc > 2) {
        output_file = argv[2];
    }

    if (argc > 3) {
        url = argv[3];
    }

    if (url.compare(0, 5, "https") != 0) {
        cout << "URL isn't https" << endl;
        return 1;
    }

    int res = action(url, certificate_file, output_file);

    if (res) return res;
    else {
        system(("openssl x509 -text -in \"" + certificate_file + "\"").data());
        cout << "Certificate is saved in " << certificate_file << ", HTTP response is saved to " << output_file << endl;
        return 0;
    }
}
