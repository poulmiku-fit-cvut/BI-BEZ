// Mikulas Poul - poulmiku

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <openssl/evp.h>

void random_text(char * text, size_t length) {
    const char charset[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    for (size_t i = 0; i < length; ++i) {
        text[i] = charset[rand() % (sizeof charset - 1)];
    }
    text[length] = 0;

}

int main(int argc, char *argv[]) {

    int i;
    char hashFunction[] = "sha256";  // zvolena hashovaci funkce ("sha1", "md5" ...)

    EVP_MD_CTX ctx;  // struktura kontextu
    const EVP_MD *type; // typ pouzite hashovaci funkce
    unsigned char hash1[EVP_MAX_MD_SIZE]; // char pole pro hash - 64 bytu (max pro sha 512)
    int length1;  // vysledna delka hashe

    srand(time(NULL));

    /* Inicializace OpenSSL hash funkci */
    OpenSSL_add_all_digests();
    /* Zjisteni, jaka hashovaci funkce ma byt pouzita */
    type = EVP_get_digestbyname(hashFunction);

    /* Pokud predchozi prirazeni vratilo -1, tak nebyla zadana spravne hashovaci funkce */
    if (!type) {
        printf("Hash %s neexistuje.\n", hashFunction);
        exit(1);
    }

    char text[35];

    int x = 0;

    while(x != 2) {

        random_text(text, 30);


        EVP_DigestInit(&ctx, type);  // nastaveni kontextu
        EVP_DigestUpdate(&ctx, text, strlen(text));  // zahashuje text a ulozi do kontextu
        EVP_DigestFinal(&ctx, hash1, (unsigned int *) &length1);  // ziskani hashe z kontextu

        if (hash1[0] == 0xAA && hash1[1] == 0xBB) {

            printf("Hash textu \"%s\" je: ", text);
            /* Vypsani vysledneho hashe */
            for (i = 0; i < length1; i++) {
                printf("%02x", hash1[i]);
            }
            printf("\n");

            ++x;
        }

    }

    exit(0);
}
