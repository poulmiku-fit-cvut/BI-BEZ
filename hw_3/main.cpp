// Mikulas Poul - poulmiku

#include <iostream>
#include <fstream>
#include <openssl/evp.h>
#include <openssl/pem.h>

using namespace std;

int action(bool encrypt, string key_path, ifstream & file, string outfile, string cipher_name) {
    OpenSSL_add_all_ciphers();
    EVP_CIPHER_CTX *ctx;
    ctx = EVP_CIPHER_CTX_new();
    if(ctx == NULL) {
        cout << "Error setting up key" << endl;
        return 2;
    }

    EVP_PKEY * evp_key;

    FILE * key_file = fopen(key_path.data(), "r");

    if (encrypt) {
        evp_key = PEM_read_PUBKEY(key_file, NULL, NULL, NULL);
    } else {
        evp_key = PEM_read_PrivateKey(key_file, NULL, NULL, NULL);
    }

    if (evp_key == NULL) {
        cout << "Error reading key." << endl;
        return 3;
    }

    fclose(key_file);

    int max_key_length = EVP_PKEY_size(evp_key);
    unsigned char * ek = new unsigned char[max_key_length];
    int ekl = 0;
    unsigned char iv[EVP_MAX_IV_LENGTH];
    int res = 0;

    uint32_t length = (uint32_t) file.tellg();
    file.seekg(0, ios::beg);

    const EVP_CIPHER * cipher_type;

    if (encrypt) {
        cipher_type = EVP_get_cipherbyname(cipher_name.data());

        if (not cipher_type) {
            cout << "Cipher " << cipher_name << " does not exist" << endl;
            return 4;
        }

        res = EVP_SealInit(ctx, cipher_type, &ek, &ekl, iv, &evp_key, 1);
    } else {
        getline(file, cipher_name);

        cipher_type = EVP_get_cipherbyname(cipher_name.data());

        if (not cipher_type) {
            cout << "Incorrect cipher in header." << endl;
            return 4;
        }

        file.read(reinterpret_cast<char *>(ek), max_key_length);
        file.read(reinterpret_cast<char *>(iv), EVP_MAX_IV_LENGTH);
        ekl = max_key_length;
        res = EVP_OpenInit(ctx, cipher_type, ek, ekl, iv, evp_key);
    }

    if (res != 1) {
        cout << "Problem with the encryption/decryption." << endl;
        cout << "During INIT" << endl;
        return 3;
    }

    ofstream temp_file;
    temp_file.open(outfile + ".tmp", ios::binary);
    uint32_t offset = (uint32_t) file.tellg();
    uint32_t block_size = 500;
    char * data = new char[block_size];
    unsigned char * cipher = new unsigned char[block_size * 3];
    int cipher_length = 0;

    // main loop
    while(offset < length) {
        uint32_t read_length = (uint32_t) min(block_size, length - offset); // handles end of file

        file.read(data, read_length);
        offset += read_length;

        if (encrypt) {
            res = EVP_SealUpdate(ctx,  cipher, &cipher_length, reinterpret_cast<unsigned char *>(data), read_length);  // encryption of orig data
        } else {
            res = EVP_OpenUpdate(ctx, cipher, &cipher_length, reinterpret_cast<unsigned char *>(data), read_length);
        }

        if(res != 1) {
            cout << "Problem with the encryption/decryption." << endl;
            cout << "During UPDATE" << endl;
            return 3;
        }

        temp_file.write(reinterpret_cast<char *>(cipher), cipher_length);
    }

    // encrypt/decrypt the rest
    if (encrypt) {
        res = EVP_SealFinal(ctx, cipher, &cipher_length);
    } else {
        res = EVP_OpenFinal(ctx, cipher, &cipher_length);
    }

    if(res != 1) {
        cout << "Problem with the encryption/decryption." << endl;
        cout << "During FINAL: " << res << endl;
        return 3;
    }

    temp_file.write(reinterpret_cast<char *>(cipher), cipher_length);
    temp_file.close();

    ifstream temp_file_read;
    temp_file_read.open(outfile + ".tmp", ios::binary);

    ofstream new_file;
    new_file.open(outfile, ios::binary);

    if (encrypt) {
        new_file << cipher_name << endl;
        new_file.write(reinterpret_cast<char *>(ek), max_key_length);
        new_file.write(reinterpret_cast<char *>(iv), EVP_MAX_IV_LENGTH);
    }

    new_file << temp_file_read.rdbuf();
    new_file.close();
    temp_file_read.close();
    remove((outfile + ".tmp").data());

    return 0;
}


int main(int argc, char * argv[]) {

    // arguments validation

    if (argc > 1 and string(argv[1]) == "--help") {
        cout << "Usage: " << endl;
        cout << "./a.out (-d|-e) <key> <input_file> <output_file> [<cipher_name='des-cbc'>]" << endl;
        cout << "Cipher name is applicable for -e only" << endl;
        return 1;
    }

    if (argc < 5 or argc > 6) {
        cout << "Invalid number of arguments, see --help for usage" << endl;
        return 1;
    }

    if (string(argv[1]) != "-d" and string(argv[1]) != "-e") {
        cout << "Invalid arguments, see --help for usage" << endl;
        return 1;
    }

    bool encrypt = string(argv[1]) == "-e";

    if (not encrypt and argc == 6) {
        cout << "Can't use cipher name option while decrypting." << endl;
        return 1;
    }

    ifstream key;
    key.open(argv[2], ios::binary | ios::ate);

    if (not key.is_open()) {
        cout << "File with key doesn't exist" << endl;
        return 1;
    }

    key.close();

    ifstream file;
    file.open(argv[3], ios::binary | ios::ate);

    if (not file.is_open()) {
        cout << "File to encrypt/decrypt doesn't exist" << endl;
        return 1;
    }

    return action(encrypt, string(argv[2]), file, string(argv[4]),
                  encrypt ? (argc == 6 ? string(argv[5]) : "des-cbc"): "");
}
